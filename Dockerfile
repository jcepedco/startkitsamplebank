# Install Polymer CLI, https://www.polymer-project.org/2.0/docs/tools/polymer-cli
FROM node

RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
	   git \
	&& rm -rf /var/lib/apt/lists/*

RUN npm install -g bower polymer-cli --unsafe-perm

# Carpeta raíz
WORKDIR /samplebankfront

# Copia de archivos
ADD . /samplebankfront

# Abrimos puerto
EXPOSE 8080

CMD ["polymer", "serve","--port","8080"]
